﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FirstGamePanel : MonoBehaviour
{
    [SerializeField] private Button _pauseButton;
    [SerializeField] private Button _exitPausePanel;

    [SerializeField] private GameObject _pausePanel;

    private int _stoppedTime = 0;
    private int _playTime = 1;

    private bool _pausePanelIsActive = false;
    private void Start()
    {
        _pauseButton.onClick.AddListener(OpenPausePanel);
        _exitPausePanel.onClick.AddListener(ExitPausePanel);
    }

    //private void Update()
    //{
    //    if (_pausePanelIsActive)
    //    {
    //        Time.timeScale = _stoppedTime;
    //    }
    //    else
    //    {
    //        Time.timeScale = _playTime;
    //    }

    //    if (Time.timeScale == _stoppedTime) return;
    //}



    private void OpenPausePanel()
    {
       
          
            _pausePanel.gameObject.SetActive(true);
            Time.timeScale = _stoppedTime;
        
      
    }

    private void ExitPausePanel()
    {
       
            Debug.Log("Siema");
            _pausePanel.gameObject.SetActive(false);
            Time.timeScale = _playTime;
      
       
    }
}
