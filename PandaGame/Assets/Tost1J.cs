﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tost1J : MonoBehaviour
{
    [SerializeField] private Animator _tostAnim;

    private void Start()
    {
        _tostAnim = gameObject.GetComponent<Animator>();
    }


    private void OnMouseDown()
    {

        _tostAnim.SetTrigger("Tost 1");
        _tostAnim.SetTrigger("Tost 2");
    }
}
