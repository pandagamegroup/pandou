﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generate : MonoBehaviour
{
    [SerializeField] private Transform _levelPart1;
    private static float _currentPositionY = 14;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Movement>())
        {
            SpawnLevel();
            _currentPositionY += 10;
        }
    }

  

    private void SpawnLevel()
    {
        Instantiate(_levelPart1, new Vector3(10, _currentPositionY, 6), Quaternion.identity);
    }


}
