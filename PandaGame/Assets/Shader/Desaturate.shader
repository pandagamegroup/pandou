﻿Shader "Unlit/Desaturate"
{
    Properties
    {
        _InputColor ("Color", Color) = (0.0, 0.0, 0.0, 0.0)
        _TextureInput("Texture", 2D) = "white" {}
        _DesaturateAmount ("DesaturateAmount", Range(-1.0, 1.0)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct VertexInput
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct VertexOutput
            {
                float4 clipSpacePos : SV_POSITION;
                float2 uv : TEXCOORD0;
            };
            
            //Main Texture input
            sampler2D _TextureInput;
            float4 _TextureInput_ST;
            
            //Color input
            float4 _InputColor;
            
            //Desaturate amount
            float _DesaturateAmount;

            VertexOutput vert (VertexInput v)
            {
                VertexOutput o;
                o.uv = TRANSFORM_TEX(v.uv, _TextureInput);
                o.clipSpacePos = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag (VertexOutput i) : SV_Target
            {
                fixed4 texCol = tex2D(_TextureInput, i.uv);
                
                float4 desaturatedValue = (float4(_InputColor.rgb, 0) * texCol) + float4(_DesaturateAmount.xxx, 0);
            
                return saturate(desaturatedValue);
            }
            ENDCG
        }
    }
}
