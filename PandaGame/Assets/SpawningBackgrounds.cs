﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningBackgrounds : MonoBehaviour
{
    [SerializeField] private GameObject _background;

    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _endOfBackground;
    private int _decreaseValueYToSpawn = 2000;

    private int _currentXLeftWall = 0;
    private float _currentZLeftWall = 21.5f;

    private float _currentY = 162.69f;

    private float _increaseValueY = 38.29f;

    private float _zEndOfWall = 21.5f;
    private int _xEndOfWall = 0;

    private void Update()
    {
        if (_player.transform.position.y >= _endOfBackground.transform.position.y - _decreaseValueYToSpawn)   // decrease value zeby zmniejszyc odleglosc end position - bawic sie tym jak daleko ma zespawnowac obiekt
        {
            SpawnLevelPart();
        }

    }

    private void SpawnLevelPart()
    {
        // zwieksza y o 4


        Instantiate(_background, new Vector3(_currentXLeftWall, _currentY, _currentZLeftWall), Quaternion.identity);

        //_levelParts[_index].transform.rotation = Quaternion.Euler(0, 0, 0); 

        // i to index tablicy game obiektów
        _currentY += _increaseValueY;

        _endOfBackground.transform.position = new Vector3(_xEndOfWall, _currentY, _zEndOfWall);



    }
}
