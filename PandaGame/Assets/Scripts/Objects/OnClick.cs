﻿using UnityEngine;
using UnityEngine.UI;


public class OnClick : MonoBehaviour
{
    [SerializeField] private GameObject _playGameTV;

    
    private void OnMouseDown()
    {
        _playGameTV.gameObject.SetActive(!_playGameTV.gameObject.activeSelf);
        
    }
}
