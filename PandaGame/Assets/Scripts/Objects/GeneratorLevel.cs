﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorLevel : MonoBehaviour
{
    [SerializeField] private GameObject[] _levelParts = new GameObject[12];
    [SerializeField] private Texture[] _prefabTextures = new Texture[3];
    [SerializeField] private GameObject _endPosition;
    private Vector3 _lastEndPosition;

    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _positionDistance;

    private float _distanceBetweenPlayerAndPlatform = 20f;

    private int _currentY = 10;
    private int _waitForSecs = 1;
    private int _index;  
    private int _timeAmount;  
    private int _currentX = 8;
    private int _currentZ = 5;
    private int _minList = 0;
    private int _maxList = 11;
    private int _increaseValueFour = 4;
    private int _decreaseValueToSpawn = 4;

    private int _whichTexture = 0;

    private int _yToSpawnNewPlatforms = 50;
    private int _yIncreaseToSpawnNew = 50;

    private int _actualPlatform = 0;

    private int _firstValueToStop = 50;
    private int _secondValueToStop = 100;

    private bool _spawningFirstPlatforms = true;
    private bool _spawningSecondPlatforms = false;
    private bool _spawningThirdPlatforms = false;
    private void Awake()
    {
       
        _lastEndPosition = _endPosition.transform.position; 
    }



    private void Update()
    {
        if (_player.transform.position.y >= _endPosition.transform.position.y - _decreaseValueToSpawn)   // decrease value zeby zmniejszyc odleglosc end position - bawic sie tym jak daleko ma zespawnowac obiekt
        {
            SpawnLevelPart();
        }
        Spawning();
    }
    private void Spawning()
    {
        if (Time.time > _timeAmount)   // spawnuje losowy obiekt gdy minie sekunda 
        {
            SpawnLevelPart();
            _timeAmount += _waitForSecs;
            _lastEndPosition = _endPosition.transform.position; // zmienia pozycje obiektu EndPosition co sekunde
        }

        //if (_endPosition.transform.position.y == _yToSpawnNewPlatforms)
        //{
        //    if (_index < 2)
        //    {
        //        _index++;
        //        _yToSpawnNewPlatforms += _yIncreaseToSpawnNew;
        //        Debug.Log(_index);
        //    }
        //    else
        //    {
        //        _index = 0;
        //        _yToSpawnNewPlatforms += _yIncreaseToSpawnNew;
        //    }
        //}
    }

    private void SpawnLevelPart()       // randomizuje index miedzy min i max, spawnuje obiekt na wektorze danym, zwieksza wartosc y i zmienia endpozycje do gory o wartosc zwiekszona
    {
        // zwieksza y o 4

          _index = Random.Range(_minList, _maxList);  
        // teraz zeby spawnowaly sie inne jezeli osiagniemy dany pułap

        //switch (_actualPlatform)
        //{
        //    case 2:  _index = 2;
        //        break;
        //    case 1: _index = 1;
        //        break;
        //    case 0: _index = 0;
        //        break;
            
        //}
      
      
           
            Instantiate(_levelParts[_index], new Vector3(_currentX, _currentY, _currentZ), Quaternion.identity);
            _currentY += _increaseValueFour;
            _endPosition.transform.position = new Vector3(_currentX, _currentY, _currentZ);
           

        
         // i to index tablicy game obiektów
        
    }
}
