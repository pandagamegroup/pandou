﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LavaPosition : MonoBehaviour
{
    private int _firstGame = 2;
    private float _speed = 2f;
    private Vector3 _temp;
    private int _wait = 3;
    private bool _lavaGoesUp = false;
    private static bool _getHit = false;

    [SerializeField] private GameObject _endPanel;

  
    private void Start()
    {
        StartCoroutine(WaitForLava());
    }
    private void Update()
    {     
        JustLava();
     
    }                                                                                             
    private IEnumerator WaitForLava()
    {
        yield return new WaitForSecondsRealtime(_wait);
        _lavaGoesUp = true;
    }
    private void JustLava()
    {
        if (_lavaGoesUp)
        {
            _temp = transform.localPosition;
            _temp.y += Time.deltaTime * _speed;
            transform.localPosition = _temp;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Movement>())
        {
            ScoreGameOne._score = 0; 
            _endPanel.gameObject.SetActive(true);
            Time.timeScale = 0;
            //  Restart();

        }
       
    }
    private void Restart()
    {
       
        SceneManager.LoadScene(_firstGame);
    }

}
