﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingJson : MonoBehaviour
{
    [SerializeField] private int[] _testArray;

    [SerializeField] private LoadJayson _systemJson;

    private void Start()
    {
        _testArray = _systemJson.HighScore;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            _systemJson.HighScore = _testArray;
        }
    }
}
