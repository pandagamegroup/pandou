﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;



public class LoadJayson : MonoBehaviour
{
    private int[] _highScore = new int[10];

    public int[] HighScore
    {
        get
        {
            return _highScore;
        }
        set
        {
            _highScore = value;
            Save();
        }
    }

    private void Awake()
    {
        Load();
    }

    private void Load()
    {
        if (File.Exists(Application.dataPath + "/save.txt"))
        {
            string saveString = File.ReadAllText(Application.dataPath + "/save.txt");
            SaveJayson loadedSaveObject = JsonUtility.FromJson<SaveJayson>(saveString);
            _highScore = loadedSaveObject.bestscores;
            
        }
    }
    private void Save()
    {
        SaveJayson saveObject = new SaveJayson { bestscores = _highScore };
        string json = JsonUtility.ToJson(saveObject);
        // zapis pliku
        File.WriteAllText(Application.dataPath + "/save.txt", json);
    }
}
