﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergySlider : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;
    private int _maxEnergyValue = 100;
    public void SetMaxEnergy(int _energy)
    {
        slider.maxValue = _maxEnergyValue;
        slider.value = _energy;

        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
        public void SetEnergy(int _energy)
    {
        slider.value = _energy;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
