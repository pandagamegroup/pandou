﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FoodSlider : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;
    private int _maxFoodValuer = 100;
    public void SetMaxFood(int _food)
    {
        slider.maxValue = _maxFoodValuer;
        slider.value = _food;

        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

  public void SetFood(int _food)
    {
        slider.value = _food;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
