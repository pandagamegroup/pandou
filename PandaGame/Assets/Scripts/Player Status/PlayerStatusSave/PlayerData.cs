﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PlayerData 
{
    
    public int Food;
    public int Energy;

    public PlayerData(PlayerStats player)
    {
        Food = player.Food;
        Energy = player.Energy;
    }
}
