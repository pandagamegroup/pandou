﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public int Food = 100;
    public int Energy = 100;
    
    Time_Deley timed;
   
    

    private bool loadeddata = false; // Sprawdzanie czy gracz wczytał gre

    PlayerStatsSave pss;
    private void Start()
    {
        
        timed = GetComponent<Time_Deley>();
        pss = GetComponent<PlayerStatsSave>();
    }

    private void Update()
    {
        Food = timed._currentFood;
        Energy = timed._currentEnergy;
    }

    public void SavePlayer() // zapis do pliku
    { 
        
        pss.SavePlayer(Food, Energy);

    }

    public void LoadPlayer() // Wczytywanie z pliku
    {
      
        Food = pss.LoadFood();
        Energy = pss.LoadEnergy();
        
        loadeddata = true; //przełączenie flagi
    }

    public void SwitchLoad()
    {
        loadeddata = false;
    }

    public bool isloaded()
    {
        return loadeddata;
    }
}
