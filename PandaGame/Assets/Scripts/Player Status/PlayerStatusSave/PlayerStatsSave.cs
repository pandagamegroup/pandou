﻿
using UnityEngine;
using System.IO;
using System;

public class PlayerStatsSave : MonoBehaviour
{
 
    
    public void SavePlayer(int food, int energy)
    {

      
        if (File.Exists(Application.persistentDataPath + "/food.txt"))
        {
            string content = food.ToString();
            File.WriteAllText(Application.persistentDataPath + "/food.txt", content);
        }
        else
        {
            File.Create(Application.persistentDataPath + "/food.txt");
            string content = food.ToString();
            File.WriteAllText(Application.persistentDataPath + "/food.txt", content);
        }
        if (File.Exists(Application.persistentDataPath+ "/energy.txt"))
        {
            string content = energy.ToString();
            File.WriteAllText(Application.persistentDataPath + "/energy.txt", content);
        }
        else
        {
            File.Create(Application.persistentDataPath + "/energy.txt");
            string content = energy.ToString();
            File.WriteAllText(Application.persistentDataPath + "/energy.txt", content);
        }
       
       
    }

    public int LoadFood()
    {
        if (File.Exists(Application.persistentDataPath + "/food.txt"))
        {
            string content = File.ReadAllText(Application.persistentDataPath + "/food.txt");
            int food = Convert.ToInt32(content);
            return food;
        }
        else
        {
            using (StreamWriter sw = File.CreateText(Application.persistentDataPath))
            {
                sw.WriteLine("100");
            }
            string content = File.ReadAllText(Application.persistentDataPath + "/food.txt");
            int food = Convert.ToInt32(content);
            return food;
        }
    }

    public int LoadEnergy()
    {
        if (File.Exists(Application.persistentDataPath + "/energy.txt"))
        {
            string content = File.ReadAllText(Application.persistentDataPath + "/energy.txt");
            int energy = Convert.ToInt32(content);
            return energy;
        }
        else
        {
            using (StreamWriter ws = File.CreateText(Application.persistentDataPath))
            {
                ws.WriteLine("100");
            }
            string content = File.ReadAllText(Application.persistentDataPath + "/energy.txt");
            int energy = Convert.ToInt32(content);
            return energy;
        }
    }
}
