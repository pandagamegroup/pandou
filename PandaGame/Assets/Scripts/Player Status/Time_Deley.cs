﻿using System;
using System.Collections;
using System.Collections.Generic;
// using UnityEditorInternal;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using UnityEngine.Events;
public class Time_Deley : MonoBehaviour
{

    PlayerStats players;
    PlayerStatsSave pSS;
    private bool _stop = true;
    private float _stopTimer;
    public bool cantSleep = false;
    public int _currentFood;
    private int maxFood;
    public int _currentEnergy;
    private int maxEnergy;

    public FoodSlider foodSlider;
    public EnergySlider energyslider;

    private int _deleyTime = 20;

    SaleScript ss;

    private bool _sleepMode;

    [SerializeField]
    private Text _Empty;

    [SerializeField]
    public GameObject _bed;
    public UnityEvent OnClick = new UnityEvent();

    void Start()
    {
        players = GetComponent<PlayerStats>();
        ss = GetComponent<SaleScript>();
        pSS = GetComponent<PlayerStatsSave>();
        maxFood = players.Food;
        maxEnergy = players.Energy;

        _currentFood = maxFood;
        _currentEnergy = maxEnergy;

        foodSlider.SetMaxFood(maxFood);
        energyslider.SetMaxEnergy(maxEnergy);

        LoadPlayer();
        
    }



    void Update()
    {
        if (players.isloaded()) // sprawdzenie czy flaga została przełączona i podstawienie wczytanych danych
        {
            maxFood = players.Food;
            maxEnergy = players.Energy;

            _currentFood = maxFood;
            _currentEnergy = maxEnergy;

            foodSlider.SetMaxFood(_currentFood);
            energyslider.SetMaxEnergy(_currentEnergy);


            players.SwitchLoad(); //przełączenie flagi 
        }
            Timer();
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == gameObject)
            {
                Debug.Log("Chuj");
                OnClick.Invoke();
            }
        }
            
    }
    //  TODO : Gdy wzkaźnik spadnie do zera timer się zatrzymuję. Trzeba wznowić po wczytaniu.
    private void Timer()
    {
            if (_stop)
            {
                StartCoroutine(FoodStatus());
                StartCoroutine(EnrgyStatus());
                _stopTimer -= Time.deltaTime;
            }
            if (_stopTimer < 0)
            {
                _stop = false;
            }
    }

    public void SleepMode()
    {
       
       if(_currentEnergy <= 100 && cantSleep == false) 
        {
            StartCoroutine(SleepStatus());
            
            _stopTimer -= Time.deltaTime;
            _sleepMode = true;
            cantSleep = true;
        }
        else
        {
            _sleepMode = false;
        }
        if (_currentEnergy >= 100)
        {
            cantSleep = false;
        }
        if (_stopTimer < 0)
        {
            _stop = false;
        }
    }

    IEnumerator FoodStatus()
    {
        while (_currentFood > 0)
        {
            yield return new WaitForSeconds(_deleyTime);
            DecreaseFood();
            pSS.SavePlayer(_currentFood, _currentEnergy);
        }
    }

    IEnumerator EnrgyStatus()
    {
       while (_currentEnergy > 0)
         {
            if (_sleepMode == false)
            {
                yield return new WaitForSeconds(_deleyTime);
                DecreaseEnergy();
            }
            else
            {
                break;
            }
                
         }
 
    }

    IEnumerator SleepStatus()
    {
        
        while (_currentEnergy <= maxEnergy)
        {
            yield return new WaitForSeconds(_deleyTime);
            IncreaseEnergy();
        }

        _sleepMode = false;
        StartCoroutine(EnrgyStatus());

    }
    public void DecreaseFood(int damage = 1)
        {

            _currentFood -= damage;
            foodSlider.SetFood(_currentFood);
            _stop = false;
        }
        
        private void DecreaseEnergy(int damage = 4)
    {
        _currentEnergy -= damage;
        energyslider.SetEnergy(_currentEnergy);
        _stop = false;
        
    }

    public void IncreaseEnergy(int recovery = 10)
    {

            _currentEnergy += recovery;
            energyslider.SetEnergy(_currentEnergy);
            _stop = false;
    }
    
    public void IncreaseFoodCarrto(int recovery = 15)
    {
        if (_currentFood >= maxFood)
        {
           
        }
        else
        {
            _currentFood += recovery;
            foodSlider.SetFood(_currentFood);
            ss._carrotValue--;
            ss.Show_Inventory();
        }
        if (ss._carrotValue == 0)
        {
            EmptyStorage();
        }
       
    }

    public void IncreaseFoodPotato(int recovery = 20)
    {
        if (_currentFood >= maxFood)
        {
            ;
        }
        else
        {
            _currentFood += recovery;
            foodSlider.SetFood(_currentFood);
            ss._potatoValue--;
            ss.Show_InventoryPotato();
        }

        if (ss._potatoValue == 0)
        {
            EmptyStorage();
        }
    }

    public void EmptyStorage()
    {
        _Empty.text = "Pusty Ekwipunek";
    }


    private void LoadPlayer()
    {
        _currentEnergy = pSS.LoadEnergy();
        _currentFood = pSS.LoadFood();
        foodSlider.SetFood(_currentFood);
        energyslider.SetEnergy(_currentEnergy);


    }   
}

