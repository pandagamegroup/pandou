﻿
using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopSystem: MonoBehaviour
{
    
    SaleScript ss;
    MoneyScript ms;
    ShopItemValue sIV;
   
    [SerializeField]
    private int _carrotPrice = 1;
    [SerializeField]
    private int _debetLimit = 20;
    [SerializeField]
    private int _potatoPrice = 3;
    [SerializeField]
    private int _strawberryPrice = 2;
    private int _gold;
    
    private void Start()
    {
       
        ms = GetComponent<MoneyScript>();
        ss = GetComponent<SaleScript>();
        sIV = GetComponent<ShopItemValue>();
    }

    //Kupowanie Marchwi
    public void Sold_Iteam()
    {


        if (ms._money > -_debetLimit + _carrotPrice - 1)//Sprawdzenie czy uzytkownik nie przekroczy debetu
        {
            ms.Decrease_Money(_carrotPrice);//Obnizenie wartosci portfela
            ss.Add_to_Inve();//Zwiększenie ilości itemów
            sIV._carrotStack = ss._carrotValue;
            sIV._money = ms._money;
            
        }
        else
        {
            ms.ShowLimit();
        }
    }
    //Kupowanie pyry
    public void Sold_Potato()
    {

        if (ms._money > -_debetLimit + _potatoPrice - 1)
        {
            ms.Decrease_Money(_potatoPrice);
            ss.AddtoInvePotato();
            sIV._potatoStack = ss._potatoValue;
            sIV._money = ms._money;
        }
        else
        {
            ms.ShowLimit();
        }
    }
    public void SoldStrawberry()
     {
        if (ms._money > -_debetLimit + _strawberryPrice - 1 )
         {
            ms.Decrease_Money(_strawberryPrice);
            ss.addToInveStrawberry();
            sIV._strawberryStack = ss._strawberryValue;
            sIV._money = ms._money;
         }
             else
                {
                    ms.ShowLimit();
                }
     }
        
    
   
   
    

    
}
