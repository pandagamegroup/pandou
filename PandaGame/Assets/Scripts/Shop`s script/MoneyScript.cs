﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MoneyScript : MonoBehaviour
{

    
    ShopItemValue sIV;
    [SerializeField]   
    private Text _gold;

    [SerializeField]
    private Text _limit;
    private string _debetText = "Osiągnięto maksymalny debet";

    public int _debetValue = 20;
    
    public int _money;
    [SerializeField]
    private Animator _pandaAnim;

    
    private void Awake()
    {
       
        sIV = GetComponent<ShopItemValue>();
        SetText();
        sIV.LoadShop();
    }

    //Pobranie wartości portfela
    public int Get_Money()
    {
        return _money;
    }
    //Zmniejszenie wartości portfela
    public void Decrease_Money(int v)
    {
        _money-=v;
        SetText();
        _pandaAnim.SetTrigger("showYes");
    }
    //Zwiekszenie wartosci portfela (W przyszłości)
    public void Increase_Money()
    {
        _money++;
        SetText();
    }

    public void SetText()
    {
        _gold.text = "Gold: " +_money;
    }

    public void ShowLimit()
    {
        if (_money <= -_debetValue)//Sprawdzanie czy uzytkownik przekroczył limit.
        {
            _limit.text = _debetText;
            _pandaAnim.SetTrigger("showNo");
        }
        else
        {
            _limit.text = " ";
           
        }

    }
}
