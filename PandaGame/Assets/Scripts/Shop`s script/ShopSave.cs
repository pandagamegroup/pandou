﻿
using UnityEngine;
using System.IO;
using System;

public class ShopSave : MonoBehaviour
{
  public void SaveShop(int _carrotV, int _potatoV, int _strawberryV, int _money)
    {
        if (File.Exists(Application.persistentDataPath + "/money.txt"))
        {
            string content = _money.ToString();
            File.WriteAllText(Application.persistentDataPath + "/money.txt", content);
        }
        else
        {
            File.Create(Application.persistentDataPath + "/money.txt");
            string content = _money.ToString();
            File.WriteAllText(Application.persistentDataPath + "/money.txt", content);
        }

        if (File.Exists(Application.persistentDataPath + "/carrotV.txt"))
        {
            string content = _carrotV.ToString();
            File.WriteAllText(Application.persistentDataPath + "/carrotV.txt", content);
        }
        else
        {
            File.Create(Application.persistentDataPath + "/carrotV.txt");
            string content = _carrotV.ToString();
            File.WriteAllText(Application.persistentDataPath + "/carrotV.txt", content);
        }

        if (File.Exists(Application.persistentDataPath + "/potatoV.txt"))
        {
            string content = _potatoV.ToString();
            File.WriteAllText(Application.persistentDataPath + "/potatoV.txt", content);
        }
        else
        {
            File.Create(Application.persistentDataPath + "/potatoV.txt");
            string content = _potatoV.ToString();
            File.WriteAllText(Application.persistentDataPath + "/potatoV.txt", content);
        }

        if (File.Exists(Application.persistentDataPath + "/strawberryV.txt"))
        {
            string content = _strawberryV.ToString();
            File.WriteAllText(Application.persistentDataPath + "/strawberryV.txt", content);
        }
        else
        {
            File.Create(Application.persistentDataPath + "/strawberryV.txt");
            string content = _strawberryV.ToString();
            File.WriteAllText(Application.persistentDataPath + "/strawberryV.txt", content);
        }
    }

    public void SaveMoney(int _money)
    {
        if (File.Exists(Application.persistentDataPath + "/money.txt"))
        {
            string content = _money.ToString();
            File.WriteAllText(Application.persistentDataPath + "/money.txt", content);
        }
        else
        {
            File.Create(Application.persistentDataPath + "/money.txt");
            string content = _money.ToString();
            File.WriteAllText(Application.persistentDataPath + "/money.txt", content);
        }
    }

    public int LoadMoney()
    {
        if (File.Exists(Application.persistentDataPath + "/money.txt"))
        {
            string content = File.ReadAllText(Application.persistentDataPath + "/money.txt");
            int _money = Convert.ToInt32(content);
            return _money;
        }
        else
        {
            return 0;
        }
    }

    public int LoadCarrot()
    {
        if (File.Exists(Application.persistentDataPath + "/carrotV.txt"))
        {
            string content = File.ReadAllText(Application.persistentDataPath + "/carrotV.txt");
            int _carrotV = Convert.ToInt32(content);
            return _carrotV;
        }
        else
        {
            return 0;
        }
    }

    public int LoadPotato()
    {
        if (File.Exists(Application.persistentDataPath + "/potatoV.txt"))
        {
            string content = File.ReadAllText(Application.persistentDataPath + "/potatoV.txt");
            int _potatoV = Convert.ToInt32(content);
            return _potatoV;
        }
        else
        {
            return 0;
        }
    }

    public int LoadStrawberry()
    {

        if (File.Exists(Application.persistentDataPath + "/strawberryV.txt"))
        {
            string content = File.ReadAllText(Application.persistentDataPath + "/strawberryV.txt");
            int _strawberryV = Convert.ToInt32(content);
            return _strawberryV;
        }
        else
        {
            return 0;
        }
    }
}
