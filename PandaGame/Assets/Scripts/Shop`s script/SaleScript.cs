﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SaleScript : MonoBehaviour
{
    ShopItemValue sIV;
    ShopSave ss;
    [SerializeField]
    private Text _ininventory;
    [SerializeField]
    private Text _inventoryPotato;
    [SerializeField]
    private Text _inventoryStrawberry;

    public int _carrotValue;
    public int _potatoValue;
    public int _strawberryValue;

    [SerializeField]
    private Text _equipmenttextcarrot;
    [SerializeField]
    private Text _equipmenttextpotato;
    [SerializeField]
    private Text _equipmentTextStrawberry;
    [SerializeField]
    private Image _imageCarrot;
    [SerializeField]
    private Image _imagePotato;
    [SerializeField]
    private Image _imageStrawberry;

    private Color _empty = Color.grey;

    private void Awake()
    {
        _imageCarrot.GetComponent<Image>();
        _imagePotato.GetComponent<Image>();
        _imageStrawberry.GetComponent<Image>();

        sIV = GetComponent<ShopItemValue>();
        Show_Inventory();
        Show_InventoryPotato();
        sIV.LoadShop();
    }

   

    public void Show_InventoryPotato()
    {
        _inventoryPotato.text = "Inventory: " + _potatoValue;
        Addtopotato();
    }

    public void Show_Inventory()
    {

        _ininventory.text = "Inventory: " + _carrotValue;
        Addtocarrot();
    }

    public void Show_InventoryStrawberry()
    {
        _inventoryStrawberry.text = "Inventory: " + _strawberryValue;
        addToInveStrawberry();

    }
    // Zwieksza liczbe przedmiotów
    public void Add_to_Inve()
    {
        _carrotValue++;
        Show_Inventory();
        
    }

    public void AddtoInvePotato()
    {
       _potatoValue++;
        Show_InventoryPotato();
    }

    public void addToInveStrawberry()
    {
        _strawberryValue++;
        Show_InventoryStrawberry();
    }

    public void Addtocarrot()
    {
        _equipmenttextcarrot.text = "In Inventory " + _carrotValue;

        if (_carrotValue == 0)
        {
            _imageCarrot.color = _empty;
        }
        else
        {
            _imageCarrot.color = Color.green;
        }
    }

    public void Addtopotato()
    {
        _equipmenttextpotato.text = "In Inventory " + _potatoValue;
        if (_potatoValue == 0)
        {
            _imagePotato.color = _empty;
        }
        else
        {
            _imagePotato.color = Color.green;
        }
    }

    public void addToStrawberry()
    {
        _equipmentTextStrawberry.text = "In Inventory " + _strawberryValue;
        if (_strawberryValue == 0)
        {
            _imageStrawberry.color = _empty;
        }
        else
        {
            _imageStrawberry.color = Color.green;
        }
    }
}

