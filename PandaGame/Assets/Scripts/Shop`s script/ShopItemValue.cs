﻿using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;

public class ShopItemValue : MonoBehaviour
{
    public int _carrotStack; 
    public int _potatoStack;
    public int _strawberryStack;
    public int _money;
   
    ShopSave shops;
    MoneyScript ms;
    SaleScript ss;
    private bool loadeddata = false;

    private void Awake()
    {
        ms = GetComponent<MoneyScript>();
        shops = GetComponent<ShopSave>();
        ss = GetComponent<SaleScript>();
        LoadShop();
    }

    

    public void SaveInventory()
    {
        shops.SaveShop(_carrotStack, _potatoStack, _strawberryStack,_money);
    }

    public void LoadShop()
    {
        
       ss._potatoValue = shops.LoadPotato();
       ss._carrotValue = shops.LoadCarrot();
       ss._strawberryValue = shops.LoadStrawberry();
       ms._money = shops.LoadMoney();
       
        loadeddata = true;
    }

    

    public void SwitchLoad()
    {
        loadeddata = false;
    }

    public bool isloaded()
    {
        return loadeddata;
    }
}   
