﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{ 

    [SerializeField] Rigidbody _myRb;
    private float _force = 10f;
    private float _speed = 10f;
    private float _speedHandheld = 15f;

    [SerializeField] private LayerMask _whatIsGround;

    [SerializeField] private SphereCollider _col;

    private bool _isGrounded = true;

    private float _moveHorizontal;
    private float _moveVertical;
    private Vector3 _movement;

    private bool _isHandheldactive = false;

    private float _timeWait = 0.3f;

    private float _changingRadiusToLower = 0.9f;

    private bool _isCoroutineActive = false;

    private void Awake()
    {
        _myRb = GetComponent<Rigidbody>();
        _col = GetComponent<SphereCollider>();

       
   
            Screen.orientation = ScreenOrientation.Portrait;     // orientacja pionowa 
            Application.targetFrameRate = 60;                    // fpsy max 60
        }

      
    
    private void FixedUpdate()
    {
      
            MovingHandheld(); 
            MovingDesktop();
        
    }
  
    private void Update()
    {
        if (!_isCoroutineActive)
        {
            StartCoroutine(WaitTimeToDoScript());
        }
    }
    private void MovingHandheld()
    {           
        _moveHorizontal = Input.acceleration.x;
        _movement = new Vector3(_moveHorizontal, 0, 0);
        _myRb.transform.Translate(_movement * _speedHandheld * Time.fixedDeltaTime);
    }
    private void MovingDesktop()
    {
      
        transform.Translate(Input.GetAxis("Horizontal") * Time.deltaTime * _speed, 0f, 0f);   // poruszamy tylko pozycją X, reszta jest na 0f
    }

    private bool IsGrounded()
        {
        return Physics.CheckCapsule(_col.bounds.center, new Vector3(_col.bounds.center.x, _col.bounds.min.y, _col.bounds.center.z), _col.radius * _changingRadiusToLower, _whatIsGround);  
        }

    // Sprawdza center kapsuły i maske _whatIsGround, jak bedzie stycznosc z nią to jest na groundzie
    
    private void Jump()
    {  
        if (IsGrounded())
        {
            _myRb.AddForce(Vector3.up * _force, ForceMode.Impulse);        
        }           
    }
    private IEnumerator WaitTimeToDoScript()    // ustawia korutyne na aktywną, czeka jakiś czas, wykonuje skok i ustawia korutyne na false
    {
        _isCoroutineActive = true;
        yield return new WaitForSeconds(_timeWait);
        Jump();
      
        _isCoroutineActive = false;
    }

}
