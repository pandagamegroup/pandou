﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Points : MonoBehaviour
{
    ShopSave shops;
    [SerializeField]
    private Text _scoreValue;
    public int _money;
    public int test;
    private void Start()
    {
        shops = GetComponent<ShopSave>();
        LoadMoney();
        showText();
        test = 0;
    }

    public void showText()
    {
        _scoreValue.text ="Ilość Monet :" +_money.ToString(); 
    }

    public void walletUp()
    {
        _money++;
        test++;
        SaveMoney();
    }

    public void SaveMoney()
    {
        shops.SaveMoney(_money);
    }

    public void LoadMoney()
    {
        _money = shops.LoadMoney();
    }
}
