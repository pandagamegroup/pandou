﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddGold : MonoBehaviour
{
    [SerializeField] private Button _addGold;

    ShopItemValue sIV;
    MoneyScript ms;
    [SerializeField]
    private Text _scoreValue;

    private bool _changedValue;
    private void Start()
    {
        ms = GetComponent<MoneyScript>();
        sIV = GetComponent<ShopItemValue>();
        _addGold.onClick.AddListener(walletUp);
    }
    

    private void Update()
    {
        if (_changedValue)
        {
            showText();
            _changedValue = false;
        }
    }

    public void showText()
    {

        _scoreValue.text = "Gold: " + ms._money; //(Do poprawy) Nie mam pojęcia jak to zrobić
    }
  
    public void walletUp()
    {
       ms._money++;

        _changedValue = true;

    }
}
