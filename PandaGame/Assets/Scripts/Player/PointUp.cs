﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointUp : MonoBehaviour
{
    

    private void OnTriggerEnter(Collider collision)
    {
        Points _walletValue = collision.GetComponent<Points>();

        if (_walletValue != null)
        {
            Object.Destroy(this.gameObject);

            _walletValue.walletUp();
            _walletValue.showText();
        }

    }
}
   
