﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody _myRb;
    private Vector3 _movement;
    private float _moveHorizontal;
    private float _moveVertical;
    [SerializeField] private float _speed = 300;
    [SerializeField] private Joystick _joystick;
    [SerializeField] private GameObject _mobileController;
    [SerializeField] private Button _jumpButton;

    private int _yourY = 0;
    private int _frameRate = 60;

    private float _force = 5f;

    private void Awake()
    {
        _jumpButton.onClick.AddListener(Jump);
        _myRb = GetComponent<Rigidbody>();

        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            _mobileController.SetActive(false);
        }
        else if (SystemInfo.deviceType == DeviceType.Handheld)
        {
            _mobileController.SetActive(true);
          //  Screen.orientation = ScreenOrientation.LandscapeLeft;
            Application.targetFrameRate = _frameRate;
        }
    }
    private void Update()
    {  // inputy update rigidbody fixedupdate

        Move();
    }
    private void Move()
    {
        _moveHorizontal = Input.acceleration.x;
        _moveVertical = Input.acceleration.y;

        _movement = new Vector3(_moveHorizontal, _yourY, _moveVertical);
        _movement = _movement.normalized;
        _myRb.AddForce(_movement * _speed * Time.fixedDeltaTime);

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            Jump();
        }
    }
    private void Jump()
    {
        _myRb.AddForce(Vector3.up * _force, ForceMode.Impulse);
    }
   
}
