﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PandaAnimation : MonoBehaviour
{
    [SerializeField] private Animator _pandaAnim;

    private void Start()
    {
        _pandaAnim = gameObject.GetComponent<Animator>();
    }

  
    private void OnMouseDown()
    {
       
        _pandaAnim.SetTrigger("Active");
    }
}
