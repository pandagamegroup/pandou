﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eating : MonoBehaviour
{
    [SerializeField] private Score _score;
    [SerializeField] private int Score = 0;
    [SerializeField] private GameObject _endGameUI;
    private float _stop = 0;
    private float _unPause = 1f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Good>())
        {
            _score.SetScore();
        }
        else if (other.GetComponent<Bad>())
        {
            Time.timeScale = _stop;
            _endGameUI.SetActive(true);
        }

    }
}
