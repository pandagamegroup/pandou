﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deployFood : MonoBehaviour
{
    [SerializeField]
    private float _spawnTime = 1f;
    //private float _speed = 0;
    private float _minX = -5;
    private float _maxX = 1;
    private float y = 13;
    private float z = 6.5f;
    [SerializeField]
    private GameObject[] _food;
    void Start()
    {
        StartCoroutine(ColorWave());
 
    }
    private void _SpawnFood(int i)
    {
        GameObject a = Instantiate(_food[i]) as GameObject;
        a.transform.position = new Vector3(Random.Range(_minX, _maxX), y, z);
    }
    IEnumerator ColorWave()
    {
        while (true)
        {
            yield return new WaitForSeconds(_spawnTime);
            _SpawnFood(Random.Range(0, _food.Length));
        }
    }
}
