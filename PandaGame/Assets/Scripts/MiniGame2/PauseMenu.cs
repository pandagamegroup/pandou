﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPaused;
    [SerializeField] private GameObject _pauseMenuUI;
    [SerializeField] private GameObject _endGameUI;
    private float _unPause = 1f;
    private float _stop = 0f;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseClick();
        }
    }
    public void PauseClick()
    {
        if (gameIsPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }
    public void Resume()
    {
        _pauseMenuUI.SetActive(false);
        _endGameUI.SetActive(false);
        Time.timeScale = _unPause;
        gameIsPaused = false;
    }
    public void Pause()
    {
        _pauseMenuUI.SetActive(true);
        Time.timeScale = _stop;
        gameIsPaused = true;
    }
    public void Home()
    {
        Time.timeScale = _unPause;
        LevelManager.Instance.Home();
    }
    public void Restart()
    {
        Resume();
        LevelManager.Instance.MiniGame2();
    }
}
