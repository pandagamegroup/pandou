﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovementMini : MonoBehaviour
{
    [SerializeField] private Button _turnLeft;
    [SerializeField] private Button _turnRight;

    [SerializeField] Rigidbody _myRb;
    private float _speed = 500f;
    private int _directionLeft = -1;
    private int _directionRight = 1;


    private void Awake()
    {
        _turnLeft.onClick.AddListener(MoveLeft);
        _turnRight.onClick.AddListener(MoveRight);
    }

    private void MoveRight()
    {
        _myRb.AddForce(Vector3.right * _speed * _directionRight);
    }

    private void MoveLeft()
    {
        _myRb.AddForce(Vector3.right * _speed * _directionLeft);
    }




}
