﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    ShopSave shops;
    //[SerializeField]
    //private GameObject _textRef;
    [SerializeField]
    private Text _scoreText;
    [SerializeField]
    private int _points;
    private void Start()
    {
        shops = GetComponent<ShopSave>();
        _points = shops.LoadMoney();
    }
    public void SetScore()
    {
        _points++;
        shops.SaveMoney(_points);
        _scoreText.text = _points.ToString();
    }

}
