﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMenu : MonoBehaviour
{
    [SerializeField] private GameObject _tutorialMenuUI;

   public void TutorialActivate()
    {
        _tutorialMenuUI.SetActive(true);
    }
    public void TutorialDisActivate()
    {
        _tutorialMenuUI.SetActive(false);
    }
}
