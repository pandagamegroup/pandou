﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform _target;  

    private float _smoothSpeed = 0.125f;
    [SerializeField] private Vector3 _offset;
    private void FixedUpdate()      
    {
        
        Vector3 desiredPosition = _target.position + _offset;              // aktualna pozycja targetu
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, _smoothSpeed);   // Prędkość smoothu     
        transform.position = smoothedPosition;
    }


}
