﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
	// Audio players components.
	[SerializeField] private AudioSource _effectsSource;
	[SerializeField] private AudioSource _musicSource;

	// Singleton instance.
	public static SoundManager Instance = null;   // zmienić - zabezpieczyć

	// Initialize the singleton instance.
	private void Awake()
	{
		// If there is not already an instance of SoundManager, set it to this.
		if (Instance == null)
		{
            Instance = this;
		}
		//If an instance already exists, destroy whatever this object is to enforce the singleton.
		else if (Instance != this)
		{
			Destroy(gameObject);
		}

		//Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad(gameObject);
	}

	// Play a single clip through the sound effects source.
	public void Play(AudioClip clip)
	{
		
		_effectsSource.clip = clip;
		_effectsSource.Play();
	}

	// Play a single clip through the music source.
	public void PlayMusic(AudioClip clip)
	{
		_musicSource.clip = clip;
		_musicSource.Play();
	}
	// stop the music
	public void StopMusic(AudioClip clip)
	{
		_musicSource.clip = clip;
		_musicSource.Stop();
	}
	// Play a random clip from an array, and randomize the pitch slightly.
	public void PauseMusic(AudioClip clip)   // pause music
	{
		_musicSource.clip = clip;
		_musicSource.Pause();
		

	}
	public void UnPauseMusic(AudioClip clip)  // unpause music
	{
		_musicSource.clip = clip;
		_musicSource.UnPause();


	}


}