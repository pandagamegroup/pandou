﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;
    public static LevelManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject(typeof(LevelManager).ToString());
                go.AddComponent<LevelManager>();
            }
            return _instance;
        }
    }
    private void Awake()
    {
        _instance = this as LevelManager;
        
    }

    private int _lvl = 1;
    private int _menu = 0;
    private int _mGame2 = 3;
    private int _mGame = 2;

    public void Home()
    {
        SceneManager.LoadScene(_lvl, LoadSceneMode.Single); ; //zmienić na indeksy
    }
    public void MainMenu()
    {
        SceneManager.LoadScene(_menu, LoadSceneMode.Single);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void MiniGame2()
    {
        SceneManager.LoadScene(_mGame2, LoadSceneMode.Single);
    }
    public void MiniGame1()
    {
        SceneManager.LoadScene(_mGame, LoadSceneMode.Single);
    
    }


}