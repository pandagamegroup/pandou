﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour
{

    //  dodanie zmiennych w unity
    //[SerializeField] private Button _previousRoomButton;
    //[SerializeField] private Button _nextRoomButton;

    [SerializeField] private Button _playGameButton;
    [SerializeField] private Button _playSecondGameButton;

    [SerializeField] private Button _exitPanelWithGamesButton;

    [SerializeField] private GameObject _panelToGame;

    private int _sleepRoom = 4;    // enum
    private int _kitchen = 3;
    private int _firstGame = 2;
    private int _secondGame = 3;


    [SerializeField] private AudioClip _musicWolf;
    [SerializeField] private AudioClip _musicLegends;


    private void Start()
    {
 

        _playGameButton.onClick.AddListener(PlayGame);
        _playSecondGameButton.onClick.AddListener(PlaySecondGame);

        _exitPanelWithGamesButton.onClick.AddListener(ExitPanel);

    }


    private void PlayGame()
    {
        SceneManager.LoadScene(_firstGame);
    }

    private void PlaySecondGame()
    {
        SoundManager.Instance.StopMusic(_musicWolf);
        SoundManager.Instance.PlayMusic(_musicLegends);
        SceneManager.LoadScene(_secondGame);
    }

    

    private void ExitPanel()
    {
        _panelToGame.gameObject.SetActive(!_panelToGame.gameObject.activeSelf);
    }

}

  
