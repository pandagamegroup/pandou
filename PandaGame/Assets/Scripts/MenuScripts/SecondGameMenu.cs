﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SecondGameMenu : MonoBehaviour
{
    [SerializeField] private Button _backButton;
    [SerializeField] private Button _resetButton;
   
    [SerializeField] private AudioClip _musicWolf;
    [SerializeField] private AudioClip _musicLegends;

    

    private void Start()
    {
       // _backButton.onClick.AddListener(BackRoom);
        _resetButton.onClick.AddListener(Reset);


    }
    private void BackRoom()
    {
        // włącza i wyłącza muzyke
        SoundManager.Instance.StopMusic(_musicLegends);
        SoundManager.Instance.PlayMusic(_musicWolf);
        LevelManager.Instance.Home();
    }

    private void Reset()
    {
        LevelManager.Instance.MiniGame2();
    }
}