﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FirstGameMenu : MonoBehaviour
{
    [SerializeField] private Button _backButton;
    [SerializeField] private Button _resetButton;

    [SerializeField] private GameObject _tutorialPanel;
    [SerializeField] private Button _openTutorial;
    [SerializeField] private Button _exitTutorial;

    private int _firstGame = 2;
    private int _gameRoom = 1;

    private void Start()
    {
        _backButton.onClick.AddListener(BackRoom);
        _resetButton.onClick.AddListener(Reset);

        _openTutorial.onClick.AddListener(OpenTutorial);
        _exitTutorial.onClick.AddListener(ExitTutorial);

    }
    private void BackRoom()
    {
        SceneManager.LoadScene(_gameRoom);
        Time.timeScale = 1;
    }

    private void Reset()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(_firstGame);
    }

    private void OpenTutorial()
    {
        _tutorialPanel.gameObject.SetActive(true);
    }
    private void ExitTutorial()
    {
        _tutorialPanel.gameObject.SetActive(false);
    }
}
 
