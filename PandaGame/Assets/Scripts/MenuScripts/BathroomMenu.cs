﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BathroomMenu : MonoBehaviour
{
    [SerializeField] private Button _previousRoomButton;
    [SerializeField] private Button _nextRoomButton;

    private int _sleepRoom = 4;
    private int _garden = 6;


    private void Start()
    {
        _previousRoomButton.onClick.AddListener(PreviousRoom);
        _nextRoomButton.onClick.AddListener(NextRoom);

    }
    private void PreviousRoom()
    {

        SceneManager.LoadScene(_sleepRoom);
    }

    private void NextRoom()
    {
        SceneManager.LoadScene(_garden);
    }
}

