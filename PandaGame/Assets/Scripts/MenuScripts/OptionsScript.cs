﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsScript : MonoBehaviour
{
    [SerializeField] private Button _buttonWithMusic;
    [SerializeField] private Button _buttonWithLanguage;
    [SerializeField] private Button _buttonWithHelp;
    [SerializeField] private Button _buttonWithAchievements;
    [SerializeField] private Button _backToOptions;

    [SerializeField] private GameObject _showMusicOptions;


    [SerializeField] private Button _exitMenuPanelButton;
    [SerializeField] private Button _openMenuPanelButton;

    [SerializeField] private GameObject _panelToOptions;

    [SerializeField] private Button _switchMusicOnButton;
    [SerializeField] private Button _switchMusicOffButton;

    [SerializeField] private Button _switchEffectsButton;

    [SerializeField] private AudioClip _musicWolf;
 

    [SerializeField] private GameObject _textMusicIsOn;
    [SerializeField] private GameObject _textMusicIsOff;

    [SerializeField] private GameObject _textEffectsIsOn;
    [SerializeField] private GameObject _textEffectsIsOff;

    private bool _isMusicActive = true;
    private bool _isEffectsActive = true;

    private bool _isPanelWithMusicActive = true;
    private bool _isPanelWithHelpActive = true;
    private bool _isPanelWithAchievementsActive = true;
    private bool _isPanelWithLanguageActive = true;
    private bool _isPanelWithBackToOptionsActive = false;

    private void Start()
    {
        _exitMenuPanelButton.onClick.AddListener(ExitMenuPanel);
        _openMenuPanelButton.onClick.AddListener(OpenMenuPanel);

        _switchMusicOnButton.onClick.AddListener(SwitchMusicOn);
        _switchMusicOffButton.onClick.AddListener(SwitchMusicOff);
        _switchEffectsButton.onClick.AddListener(SwitchEffects);

        _buttonWithMusic.onClick.AddListener(PanelWithMusic);
        _buttonWithLanguage.onClick.AddListener(PanelWithLanguage);
        _buttonWithHelp.onClick.AddListener(PanelWithHelp);
        _buttonWithAchievements.onClick.AddListener(PanelWithAchievements);
        _backToOptions.onClick.AddListener(BackToOptions);
    }

  

    private void OpenMenuPanel()
    {
        _panelToOptions.gameObject.SetActive(!_panelToOptions.gameObject.activeSelf);
    }

    private void ExitMenuPanel()
    {
        _panelToOptions.gameObject.SetActive(!_panelToOptions.gameObject.activeSelf);
    }

    private void SwitchMusicOn()
    {
        if (_isMusicActive)
        {
            SoundManager.Instance.PauseMusic(_musicWolf);
            _isMusicActive = false;
            _textMusicIsOn.gameObject.SetActive(false);
            _textMusicIsOff.gameObject.SetActive(true);

        }

    }

    private void SwitchMusicOff()
    {



        if (!_isMusicActive)
        {
            SoundManager.Instance.UnPauseMusic(_musicWolf);
            _isMusicActive = true;
            _textMusicIsOn.gameObject.SetActive(true);
            _textMusicIsOff.gameObject.SetActive(false);
        }
    }

    private void SwitchEffects()
    {
        if (_isEffectsActive)
        {
            _textEffectsIsOn.gameObject.SetActive(false);
            _textEffectsIsOff.gameObject.SetActive(true);
            _isEffectsActive = false;
        }
        else if (!_isEffectsActive)
        {
            _textEffectsIsOn.gameObject.SetActive(true);
            _textEffectsIsOff.gameObject.SetActive(false);
            _isEffectsActive = true;
        }
    }

    private void PanelWithMusic()  
    {
        PanelWithMusicActive();
 
    }

    private void PanelWithLanguage()
    {
        IsPanelWithLanguageActive();
    
    }

    private void PanelWithHelp()
    {
        IsPanelWithHelpActive();
     
    }

    private void PanelWithAchievements()
    {
        IsPanelWithAchievementsActive();
  
    }

    private void BackToOptions()
    {
        IsPanelWithBackToOptionsActive();
       
    }

    private void PanelWithMusicActive()
    {
        // z 2 metod zrobić jedna z parametrami, zamiast ifów enumy bool music, bool achievements, bool language, bool help
        if (_isPanelWithMusicActive)
        {

            _isPanelWithMusicActive = true;
            _isPanelWithLanguageActive = false;
            _isPanelWithHelpActive = false;
            _isPanelWithAchievementsActive = false;
            _isPanelWithBackToOptionsActive = true;

            _showMusicOptions.gameObject.SetActive(true);

            _buttonWithMusic.gameObject.SetActive(true);
            _buttonWithLanguage.gameObject.SetActive(false);
            _buttonWithHelp.gameObject.SetActive(false);
            _buttonWithAchievements.gameObject.SetActive(false);

            _backToOptions.gameObject.SetActive(true);
        }             
    }
    private void IsPanelWithHelpActive()
    {
        if (_isPanelWithHelpActive)
        {
            _isPanelWithMusicActive = false;
            _isPanelWithLanguageActive = false;
            _isPanelWithHelpActive = true;
            _isPanelWithAchievementsActive = false;
            _isPanelWithBackToOptionsActive = true;
            _buttonWithMusic.gameObject.SetActive(false);
            _buttonWithLanguage.gameObject.SetActive(false);
            _buttonWithHelp.gameObject.SetActive(true);
            _buttonWithAchievements.gameObject.SetActive(false);
            _backToOptions.gameObject.SetActive(true);
        }
    }
    private void IsPanelWithAchievementsActive()
    {

        if (_isPanelWithAchievementsActive)
        {
            _isPanelWithMusicActive = false;
            _isPanelWithLanguageActive = false;
            _isPanelWithHelpActive = false;
            _isPanelWithAchievementsActive = true;
            _isPanelWithBackToOptionsActive = true;


            _buttonWithMusic.gameObject.SetActive(false);
            _buttonWithLanguage.gameObject.SetActive(false);
            _buttonWithHelp.gameObject.SetActive(false);
            _buttonWithAchievements.gameObject.SetActive(true);

            _backToOptions.gameObject.SetActive(true);
        }

    }
    private void IsPanelWithLanguageActive()
    {

        if (_isPanelWithLanguageActive)
        {
            _isPanelWithMusicActive = false;
            _isPanelWithLanguageActive = true;
            _isPanelWithHelpActive = false;
            _isPanelWithAchievementsActive = false;
            _isPanelWithBackToOptionsActive = true;


            _buttonWithMusic.gameObject.SetActive(false);
            _buttonWithLanguage.gameObject.SetActive(true);
            _buttonWithHelp.gameObject.SetActive(false);
            _buttonWithAchievements.gameObject.SetActive(false);

            _backToOptions.gameObject.SetActive(true);
        }
    }

    private void IsPanelWithBackToOptionsActive()
    {

        if (_isPanelWithBackToOptionsActive)
        {
            _isPanelWithMusicActive = true;
            _isPanelWithLanguageActive = true;
            _isPanelWithHelpActive = true;
            _isPanelWithAchievementsActive = true;
            _isPanelWithBackToOptionsActive = false;


            _buttonWithMusic.gameObject.SetActive(true);
            _buttonWithLanguage.gameObject.SetActive(true);
            _buttonWithHelp.gameObject.SetActive(true);
            _buttonWithAchievements.gameObject.SetActive(true);

            _backToOptions.gameObject.SetActive(false);
            _showMusicOptions.gameObject.SetActive(false);
        }
    }

}