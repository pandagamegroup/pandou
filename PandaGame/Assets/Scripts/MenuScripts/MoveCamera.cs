﻿using UnityEngine;
using UnityEngine.UI;

public class MoveCamera : MonoBehaviour
{
    [SerializeField] private Button _previousRoomButton;
    [SerializeField] private Button _nextRoomButton;
    [SerializeField] private GameObject _camera;
    [SerializeField] private GameObject[] _firstWaypoint = new GameObject[5];   // tablica obiektów w unity - empty gameobject
    [SerializeField] private GameObject _previousButton;
    [SerializeField] private GameObject _nextButton;
    [SerializeField] private GameObject _sleepButton;

    [SerializeField] private int i=1;
    private int _maxIndex = 4;
    private void Start()
    {
        _previousRoomButton.onClick.AddListener(PreviousRoom);  // przypisanie onClick do guzików
        _nextRoomButton.onClick.AddListener(NextRoom);
        SleepButtonOff();


    }
    private void PreviousRoom()          // jak klikne to zmniejsza sie wartosc enuma razem z waypointem
    {
        i--;     
        _camera.gameObject.transform.position = _firstWaypoint[i].gameObject.transform.position;   // zmienia pozycje kamery patrząc na [i] obiektu
        if (i == 0)
        {
            _previousButton.gameObject.SetActive(false);
        }
        if (i < _maxIndex)
        {
            _nextButton.gameObject.SetActive(true);
        }
        SleepButtonOff();
    }
    private void NextRoom()   // zwieksza tą wartosc
    {
        i++;
      
        _camera.gameObject.transform.position = _firstWaypoint[i].gameObject.transform.position;  // zmienia pozycje kamery na punkt na mapie z [indexu]

        if (i == _maxIndex)
        {
            _nextButton.gameObject.SetActive(false);
        }
         if(i>0)
        {
            _previousButton.gameObject.SetActive(true);
        }
        SleepButtonOff();

    }

    private void SleepButtonOff()   // wylacza przycisk od spanka
    {
        if (i == 2)
        {

            _sleepButton.gameObject.SetActive(true);
        }
        else
        {
            _sleepButton.gameObject.SetActive(false);
        }
    }
}
