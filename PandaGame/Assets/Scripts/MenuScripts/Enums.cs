﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums : MonoBehaviour
{
    public enum Rooms
    {
        kitchen = 1,
        gameroom = 2,
        sleeproom = 3,
        bathroom = 4,
        garden = 5,
    }
    [SerializeField] private Rooms _actualRooms;
   
}
