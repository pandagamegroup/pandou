﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SleepMenu : MonoBehaviour
{
    [SerializeField] private Button _previousRoomButton;
    [SerializeField] private Button _nextRoomButton;

    private int _gameRoom = 0;
    private int _bathRoom = 5;

    private void Start()
    {
        _previousRoomButton.onClick.AddListener(PreviousRoom);
        _nextRoomButton.onClick.AddListener(NextRoom);

    }
    private void PreviousRoom()
    {

        SceneManager.LoadScene(_gameRoom);
    }

    private void NextRoom()
    {
        SceneManager.LoadScene(_bathRoom);
    }
}
 
