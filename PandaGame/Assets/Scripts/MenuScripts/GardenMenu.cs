﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GardenMenu : MonoBehaviour
{
    [SerializeField] private Button _previousRoomButton;

    private int _bathRoom = 5;

    void Start()
    {
        _previousRoomButton.onClick.AddListener(PreviousRoom);
       

    }
    void PreviousRoom()
    {

        SceneManager.LoadScene(_bathRoom);
    }

}
 
