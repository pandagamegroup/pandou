﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class KitchenMenu : MonoBehaviour
{
 
    [SerializeField] private Button _nextRoomButton;
    [SerializeField] private Button _openShopButton;
   

    [SerializeField] private GameObject shopPanel;

    private int _gameRoom = 0;
    private void Start()
    {
       
        _nextRoomButton.onClick.AddListener(NextRoom);
        _openShopButton.onClick.AddListener(OpenShopPanel);
  
    }
  

    private void NextRoom()
    {
        SceneManager.LoadScene(_gameRoom);
    }

    private void OpenShopPanel()
    {
        shopPanel.gameObject.SetActive(!shopPanel.gameObject.activeSelf);
    }

    

}
 
