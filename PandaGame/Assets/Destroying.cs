﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroying : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<DestroyPrefabs>())
        {
            Destroy(this.gameObject);
        }
    }
}
