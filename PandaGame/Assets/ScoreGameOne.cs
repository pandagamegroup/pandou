﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreGameOne : MonoBehaviour
{
   // [SerializeField] private GameObject _player;
    [SerializeField] private TextMeshProUGUI _actualScore;
   
    public static int _score = 0;
    private static int _addScore = 100;

    private void Awake()
    {
        _actualScore.text = "Score: " + _score;
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<ThroughPlatforms>())
        {
            _score = _score + _addScore;
            _actualScore.text = "Score: " + _score;         
        }
    }
}
