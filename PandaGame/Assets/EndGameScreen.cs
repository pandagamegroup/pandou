﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndGameScreen : MonoBehaviour
{
    [SerializeField] private Button _restart;
    [SerializeField] private Button _home;

    private void Awake()
    {
        _restart.onClick.AddListener(Restart);
        _home.onClick.AddListener(Home);
    }

    private void Restart()
    {
        Time.timeScale = 1;
        LevelManager.Instance.MiniGame1();


    }

    private void Home()
    {
        LevelManager.Instance.Home();
    }
}
