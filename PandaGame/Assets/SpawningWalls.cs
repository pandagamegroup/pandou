﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningWalls : MonoBehaviour
{
    [SerializeField] private GameObject _leftWall;
    [SerializeField] private GameObject _rightWall;
    [SerializeField] private GameObject _player;
    [SerializeField] private GameObject _endOfWall;
    private int _decreaseValueYToSpawn = 20;

    private int _currentXLeftWall = -3;
    private int _currentZLeftWall = -10;

    private int _currentY = 40;

    private int _currentXRightWall = 21;
    private int _currentZRightWall = -10;

    private int _increaseValueY = 20;

    private int _zEndOfWall = 0;
    private int _xEndOfWall = 4;

    private void Update()
    {
        if (_player.transform.position.y >= _endOfWall.transform.position.y - _decreaseValueYToSpawn)   // decrease value zeby zmniejszyc odleglosc end position - bawic sie tym jak daleko ma zespawnowac obiekt
        {
            SpawnLevelPart();
        }
       
    }

    private void SpawnLevelPart()
    {
        // zwieksza y o 4

        
        Instantiate(_leftWall, new Vector3(_currentXLeftWall, _currentY, _currentZLeftWall), Quaternion.identity);
        Instantiate(_rightWall, new Vector3(_currentXRightWall, _currentY, _currentZRightWall), Quaternion.identity);

      

        //_levelParts[_index].transform.rotation = Quaternion.Euler(0, 0, 0); 

        // i to index tablicy game obiektów
        _currentY += _increaseValueY;

      _endOfWall.transform.position = new Vector3(_xEndOfWall, _currentY, _zEndOfWall);



    }
}
