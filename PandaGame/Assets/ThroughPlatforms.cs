﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThroughPlatforms : MonoBehaviour
{
    [SerializeField] private GameObject _platformCollider;

    private BoxCollider _platformBoxCollider;
    private float _platformPosY;



    private void Awake()
    {
        _platformBoxCollider = _platformCollider.GetComponent<BoxCollider>();
        _platformPosY = _platformCollider.transform.position.y;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Movement>())
        {
           _platformBoxCollider.isTrigger = true;
        
        }   
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.GetComponent<Movement>())
        {
            if (other.transform.position.y > _platformPosY)
            {
                _platformBoxCollider.isTrigger = false;
            }      
        }
       
    }

}
